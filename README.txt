Overview
--------
This module allows users to manage their own contact list in drupal. Some 
constraints can be applied like max number of contacts per user, private or 
public directories and administration permissions.


Requirements
------------
Drupal 4.5.x
PHP 4.3.0 or greater



Features
--------
 - Administrative features:
   o Can customize block title 
   o Can define the type of directory (public/private) 
   o Limit contacts per user 
   o Can customize block title
   o Can add/edit/delete contacts
   o Can search/list contacts
   
 - User features:
   o Can add/edit/delete own directory if have 'manage contacts' permission
   o Can add/edit/delete directories if have 'administer contacts' permission
   o Can search/list directories if have 'use contacts' permission
   o Can search/list directories if have 'view contacts' permission
   o Can view full contact information if have 'use contacts' permission
   o Can view public information if have 'view contacts' permission
   


Installation
------------
  1. Create the SQL tables. This depends a little on your system, but the most
     common method is:
        mysql -u username -ppassword drupal < contact_dir.mysql
  2. Copy contact_dir folder to modules/.



Configuration
-------------
  1. Modify the fields.inc file to custom your 
  2. Enable the module as usual from Drupal's admin pages.
  3. Under settings, configure the contact directory settings.
  4. Grant permissions to the groups.
     'administer contacts' full administration for all contacts
     'manage contacts' full administration for own contacts
     'use contacts' search and full view of contacts
     'view contacs' restricted view of contacts' information


   
Credits
-------
LatPro Inc.

 - Core functionality originally written by:
    Jorge Tite
   