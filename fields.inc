<?php
/**
 * Contact Fileds
 *
 * Specifies any fields that are applicable for contacts.
 *
 * @version 1.1
 * @author  LatPro Inc (George)
 */

function _contact_dir_fields() {
	$fields = _contact_dir_fields_definitions();

	// omit any fields which are not permitted for current user.
	foreach ($fields as $field => $def) {
		if (!$def[5]) {
			unset($fields[$field]);
		}
	}
	return $fields;
}

function _contact_dir_fields_definitions() {
	$fields = array();
	$access = user_access('administer contacts') || user_access('manage contacts') || user_access('use contacts');

	// Default fileds
	$fileds['category'] = array(
		'select',					// field type
		t('Primary category'), 				// display name
		true, 						// required
		true, 						// store in separate db column
		array(true, ''),			// show in view
		1, 							// permission
		'',							// default value
		_contact_dir_get_select_category(true),		//options
		false,						// multiple
		t('contact\'s primary category'), 	// Help Text
		NULL);						// extra

	$fileds['first_name'] = array(
		'textfield',        	// Field type
		t('First name'),      	// Display name
		true,              		// Required field
		true,               	// Store in separate database field
		array(false, ''),      	// Show in view
		1,                 		// permission
		'',                 	// Default value
		40, 					// Field length
		100,                	// Maximum input length
		t('contact\'s first name'),	// Help text
		NULL);

	$fileds['middle_name'] = array(
		'textfield',        	// Field type
		t('Middle name'),      	// Display name
		false,              	// Required field
		true,               	// Store in separate database field
		array(false, ''),      	// Show in view
		1,                 		// permission
		'',                 	// Default value
		40, 					// Field length / options
		100,                	// Maximum input length
		t('contact\'s middle name'),	// Help text
		NULL);

	$fileds['last_name'] = array(
		'textfield',        	// Field type
		t('Last name'),      	// Display name
		true,              		// Required field
		true,               	// Store in separate database field
		array(false, ''),      	// Show in view
		1,                 		// permission
		'',                 	// Default value
		40, 					// Field length / options
		100,                	// Maximum input length
		t('contct\'s last name'),	// Help text
		NULL);

	$fileds['mail'] = array(
		'textfield',        	// Field type
		t('E-mail'),      		// Display name
		true,              		// Required field
		true,               	// Store in separate database field
		array(true, 'style="color: red;"'),       	// Show in view
		$access,                	// permission
		'',                 	// Default value
		40, 					// Field length / options
		250,                	// Maximum input length
		t('contact\'s e-mail address'),	// Help text
		NULL);

	$fileds['company'] = array(
		'textfield',        	// Field type
		t('Company'),      		// Display name
		true,              		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,                	// permission
		'',                 	// Default value
		40, 					// Field length / options
		250,                	// Maximum input length
		t('contact\'s company name'),	// Help text
		NULL);

	$fileds['country'] = array(
		'select',				// field type
		t('Country'), 			// display name
		true, 					// required
		true, 					// store in separate db column
		array(true, ''),       	// Show in view
		$access,				// permission
		'',						// default value
		_get_contact_country_list(),	// options
		false,					// multiple
		t('contact\'s country'), 		// Help Text
		NULL);					// extra

	$fileds['state'] = array(
		'select',				// field type
		t('State'), 			// display name
		true, 					// required
		true, 					// store in separate db column
		array(true, ''),       	// Show in view
		$access, 				// permission
		'',						// default value
		_get_contact_state_list(),		// options
		false,					// multiple
		t('contact\'s state'), 		// Help Text
		NULL);					// extra

	$fileds['city'] = array(
		'textfield',        	// Field type
		t('City'),      		// Display name
		true,              		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		40, 					// Field length / options
		150,                	// Maximum input length
		t('contact\'s city'),	// Help text
		NULL);

	$fileds['address'] = array(
		'textfield',        	// Field type
		t('Address'),      		// Display name
		false,              	// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		60, 					// Field length / options
		250,                	// Maximum input length
		t('contact\'s address'),	// Help text
		NULL);

	$fileds['zip'] = array(
		'textfield',        	// Field type
		t('Zip'),      			// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		10, 					// Field length / options
		10,                		// Maximum input length
		t('contact\'s zip code'),	// Help text
		NULL);

	$fileds['phone'] = array(
		'textfield',        	// Field type
		t('phone'),      		// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		40, 					// Field length / options
		50,                		// Maximum input length
		t('contact\'s phone number'),	// Help text
		NULL);

	$fileds['fax'] = array(
		'textfield',        	// Field type
		t('fax'),      			// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		40, 					// Field length / options
		50,                		// Maximum input length
		t('contact\'s fax number'),	// Help text
		NULL);

	$fileds['url'] = array(
		'url',        			// Field type
		t('Url'),      			// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,              	// permission
		'',                 	// Default value
		60, 					// Field length / options
		255,                	// Maximum input length
		t('contact\'s url'),	// Help text
		NULL);

	$fileds['job'] = array(
		'textfield',       		// Field type
		t('Job'),      			// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,               	// permission
		'',                 	// Default value
		40, 					// Field length / options
		100,                	// Maximum input length
		t('contact\'s job/occupation'),	// Help text
		NULL);

	$fileds['description'] = array(
		'textarea',       		// Field type
		t('Description'),      	// Display name
		false,             		// Required field
		true,               	// Store in separate database field
		array(true, ''),       	// Show in view
		$access,                 	// permission
		'',                 	// Default value
		60, 					// Field length / options
		4,                	// Maximum input length
		t('contact\'s description/services (max 1000 chars)'),	// Help text
		NULL);

	return $fileds;
}


/**
 * Returns a list of countries as an array
 */
function _get_contact_country_list() {
	$countries = array();

	$countries['0'] = '[Select]';
	$countries['UNITED STATES'] = 'UNITED STATES';
	$countries['CANADA'] = 'CANADA';

	return $countries;
}

/**
 * Returns a list of states as an array
 */
function _get_contact_state_list() {
	$states = array();

	$states['0'] = '[Select]';
	$states['AK'] = 'AK';
	$states['AS'] = 'AS';
	$states['AZ'] = 'AZ';
	$states['AR'] = 'AR';
	$states['CA'] = 'CA';
	$states['CO'] = 'CO';
	$states['CT'] = 'CT';
	$states['DE'] = 'DE';
	$states['DC'] = 'DC';
	$states['FL'] = 'FL';
	$states['FM'] = 'FM';
	$states['GA'] = 'GA';
	$states['GU'] = 'GU';
	$states['HI'] = 'HI';
	$states['ID'] = 'ID';
	$states['IL'] = 'IL';
	$states['IN'] = 'IN';
	$states['IA'] = 'IA';
	$states['KS'] = 'KS';
	$states['KY'] = 'KY';
	$states['LA'] = 'LA';
	$states['ME'] = 'ME';
	$states['MH'] = 'MH';
	$states['MD'] = 'MD';
	$states['MA'] = 'MA';
	$states['MI'] = 'MI';
	$states['MN'] = 'MN';
	$states['MS'] = 'MS';
	$states['MO'] = 'MO';
	$states['MT'] = 'MT';
	$states['NE'] = 'NE';
	$states['NV'] = 'NV';
	$states['NH'] = 'NH';
	$states['NJ'] = 'NJ';
	$states['NM'] = 'NM';
	$states['NY'] = 'NY';
	$states['NC'] = 'NC';
	$states['ND'] = 'ND';
	$states['MP'] = 'MP';
	$states['OH'] = 'OH';
	$states['OK'] = 'OK';
	$states['OR'] = 'OR';
	$states['PW'] = 'PW';
	$states['PA'] = 'PA';
	$states['PR'] = 'PR';
	$states['RI'] = 'RI';
	$states['SC'] = 'SC';
	$states['SD'] = 'SD';
	$states['TN'] = 'TN';
	$states['TX'] = 'TX';
	$states['UT'] = 'UT';
	$states['VT'] = 'VT';
	$states['VI'] = 'VI';
	$states['VA'] = 'VA';
	$states['WA'] = 'WA';
	$states['WV'] = 'WV';
	$states['WI'] = 'WI';
	$states['WY'] = 'WY';
	$states['OUTSIDE USA'] = 'OUTSIDE USA';

	return $states;
}

?>