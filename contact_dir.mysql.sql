#
# Table structure for table 'contact_category'
#

DROP TABLE IF EXISTS contact_category;
CREATE TABLE `contact_category` (
  `cid` int(10) unsigned NOT NULL default '0',
  `name` varchar(100) NOT NULL default '0',
  PRIMARY KEY  (`cid`)
) TYPE=MyISAM;



#
# Table structure for table 'contact_directory'
#

DROP TABLE IF EXISTS contact_directory;
CREATE TABLE `contact_directory` (
  `cid` int(10) unsigned NOT NULL default '0',
  `uid` int(10) unsigned NOT NULL default '0',
  `created` int(11) NOT NULL default '0',
  `changed` int(11) NOT NULL default '0',
  `rating` int(11) NOT NULL default '0',
  `category` int(10) unsigned NOT NULL default '0',
  `first_name` varchar(100) default NULL,
  `middle_name` varchar(50) default NULL,
  `last_name` varchar(100) default NULL,
  `mail` varchar(250) default NULL,
  `company` varchar(150) default NULL,
  `country` varchar(150) default NULL,
  `state` varchar(150) default NULL,
  `city` varchar(150) default NULL,
  `address` varchar(250) default NULL,
  `zip` varchar(10) default NULL,
  `phone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `url` varchar(255) default NULL,
  `job` varchar(100) default NULL,
  `description` tinytext,
  PRIMARY KEY  (`cid`)
) TYPE=MyISAM;

